<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    public function testExample()
    {
        $this->get('/users')
            ->assertStatus(200)
            ->assertSee('Lista de usuarios')
            ->assertSee('Pepe')
            ->assertSee('pepe@daw.com')
            ->assertSee('administrador')
            ->assertSee('Sonia')
            ->assertSee('sonia@daw.com')
            ->assertSee('usuario');
    }
}
