<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/users', 'API\UserController@index');
Route::resource('/articles', 'API\ArticleController', ['except' => ['create', 'edit']]);
Route::post('register', 'API\AuthController@register');
