<?php

Route::get('/', function ()
{
    return view('welcome');
});

Route::resource('/roles', 'RoleController');
Route::resource('/reservations', 'ReservationController');
Route::resource('/users', 'UserController');
Route::resource('/articles', 'ArticleController');


Auth::routes();

Route::get('/articles/remember/{id}', 'ArticleController@remember');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/sesion/customer', 'SessionController@customer');
Route::post('/sesion/customer', 'SessionController@storeCustomer');

Route::get('/sesion/flush', 'SessionController@flush');
Route::get('/sesion/forget', 'SessionController@forget');
Route::get('/sesion/forget_customer/{key}', 'SessionController@forgetCustomer');
Route::get('/sesion/forget_full_customer', 'SessionController@forgetFullcustomer');


Route::get('/sesion/sample', 'SessionController@sample');
Route::post('/sesion/sample', 'SessionController@samplePost');
Route::get('/sesion/sample_forget', 'SessionController@sampleForget');
Route::get('/sesion/sample_forgetArray/{key}', 'SessionController@sampleForgetArray');

Route::get('/redirect/google', 'SocialAuthController@redirect');
Route::get('/callback/google', 'SocialAuthController@callback');

Route::get('mail/basket','MailControler@basket');
