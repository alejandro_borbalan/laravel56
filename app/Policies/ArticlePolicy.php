<?php

namespace App\Policies;

use App\User;
use App\Article;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->isAdmin())
        {
            return true;
        }
    }

    public function view(User $user, Article $article)
    {
        return true;
    }

    public function create(User $user)
    {
        dd($user);
        return $user->isAdmin();
    }

    /*actualizar los pares*/
    public function update(User $user, Article $article)
    {
        return !($article->id % 2);
    }

    /*borrar los divisibles entre 4*/
    public function delete(User $user, Article $article)
    {
        return !($article->id % 4);
    }

    public function index(User $user)
    {
        return true;
    }
}
