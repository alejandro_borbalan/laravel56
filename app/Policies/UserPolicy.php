<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function view(User $user, User $model)
    {

    }

    public function create(User $user)
    {

    }

    public function update(User $user, User $model)
    {

    }

    public function delete(User $user, User $model)
    {

    }

    public function index(User $user)
    {
        return $user->isAdmin();
    }
}
