<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $articles = Article::paginate(5);
        return view('articles.index', ['articles' => $articles]);
    }

    public function create()
    {
        return view('articles.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,
        [
            'code' => 'required|unique:articles|max:6',
            'name' => 'required|max:255',
            'price' => 'required|numeric'
        ]);

        $this->authorize('create', \App\Article::class);

        $article = new Article();
        $article->fill($request->all());
        $article->code = $request->input('code');
        $article->save();
        return redirect('/articles');
    }

    public function show($id)
    {
        $article = Article::find($id);
        $this->authorize('view', $article);

        return view('articles.show', ['article' => $article]);
    }

    public function edit($id)
    {
        $article = Article::where('id', $id)->first();
        $this->authorize('update', $article);
        return view('articles.edit', ['article' => $article]);
    }

    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        $this->authorize('update', $article);
        $article->fill($request->all());
        $article->save();
        return redirect('/articles');
    }

    public function destroy($id)
    {
        $article = Article::find($id);
        $this->authorize('delete', $article);

        $article->delete();
        return redirect('/articles');
    }

    public function remember($id)
    {
        $article = Article::find($id);
        session()->push('articles', $article);


        if (session()->has("basket.$id")) {
            $amount = session()->get("basket.$id.amount");
            $amount++;
            session()->put("basket.$id.amount", $amount);
        } else {
            session()->put("basket.$id.article", $article);
            session()->put("basket.$id.amount", 1);
        }
        return back();
    }
}
