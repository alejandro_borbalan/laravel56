<?php

namespace App\Http\Controllers;
use App\Reservation;
use App\Resource;
use App\User;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function __construct()
    {
    }
    public function index()
    {
        $reservations = Reservation::all();
        return view('reservations.index',["reservations" => $reservations]);
    }

    public function create()
    {
        $resources = Resource::all();
        $users = User::all();
        return view('reservations.create',["resources" => $resources, "users" => $users]);
    }

    public function store(Request $request)
    {
         $reservation = new Reservation();
         $reservation->user_id = $request->user_name;
         $reservation->date = $request->input("start");
         $reservation->limit = $request->input("end") ;
         $reservation->save();

         foreach ($request->input("resources") as $key )
         {
            $resource = Resource::find($key);
            $resource->reservation_id = $reservation->id;
            $resource->save();
        }
         return redirect("/reserves");
    }

    public function show($id)
    {
        $reservation = Reservation::find($id);

        return view('reservations.show', ['reservation' => $reservation]);
    }

    public function edit($id)
    {
        $reservation = Reservation::find($id);
        return view('reservations.edit',["reservation" => $reservation]);
    }

    public function update(Request $request, $id)
    {
        $reservation = Reservation::find($id);
        $reservation->limit = $request->input("limit") ;
        $reservation->save();
        return redirect("/reservations");
    }

    public function destroy($id)
    {
        $reservation = Reservation::find($id);
        foreach ($reservation->resources as $resource )
        {
            dd($resource->id);
            $resource = Resource::find($resource->id);

        }

       $reservation->delete();

        return redirect("/reservations");
    }
}
