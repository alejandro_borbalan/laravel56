<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->authorize('index', User::class);

        $users = User::with('role')->paginate(5);

        return view('users.index', ['users' => $users]);
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',["user" => $user]);
    }
    public function edit($id)
    {

    }

    public function update(Request $request, $id)
    {

    }
    public function destroy($id)
    {
        $user = User::find($id);;

        $user->delete();
        return redirect('/users');
    }
}
