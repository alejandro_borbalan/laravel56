<?php

namespace App\Http\Controllers;
use App\Resource;

use Illuminate\Http\Request;

class ResourceController extends Controller
{
    public function index()
    {
        $resources = Resource::all();
        return view('resources.index',["resources" => $resources]);
    }

    public function create()
    {
        return view('resources.create');
    }

    public function store(Request $request)
    {
         $resource = new Resource();
         $resource->fill($request->all());
         $resource->save();
         return redirect("/resources");
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $resource = Resource::find($id);
        return view('resources.edit',["resource" => $resource]);
    }

    public function update(Request $request, $id)
    {
         $resource = Resource::find($id);
         $resource->fill($request->all());
         $resource->save();
         return redirect("/resources");
    }

     public function destroy($id)
    {
        $resource = Resource::find($id);
        $resource->delete();

        return redirect("/resources");
    }
}
