<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::all();
        return $articles;
    }
    public function store(Request $request)
    {
        $article = new Article();
        $article->fill($request->all());
        $article->code = $request->input('code');
        $article->save();
        return ['done' => true];
    }

    public function show($id)
    {
        $article = $this->findOrFail($id);
        return $article;
    }

    public function update(Request $request, $id)
    {
        $article = $this->findOrFail($id);
        $article->fill($request->all());
        $article->save();
        return ['updated' => $id];
    }

    public function destroy($id)
    {
        $article = $this->findOrFail($id);
        $article->delete();

        return ['deleted' => $id];
    }

    private function findOrFail($id)
    {
        $article = Article::find($id);
        if (!$article) {
            return response()->json([
                'message' => 'Record not found',
            ], 404);
        }
        return $article;
    }
}
