<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function customer()
    {
        return view('session.customer');
    }

    public function storeCustomer(Request $request)
    {
        $request->session()->put('customer.name', $request->input('name'));
        $request->session()->put('customer.surname', $request->input('surname'));

        return back();
        return redirect('/home');
    }

    public function flush(Request $request)
    {
        $request->session()->flush();
        return back();
    }

    public function forget(Request $request)
    {
        $request->session()->forget('text');

        $request->session()->forget('arraytext');

        return back();
        return redirect('/home');
    }


    public function sample()
    {
        return view('session.sample');
    }

    public function samplePost(Request $request)
    {
        $text = $request->input('text');

        session(['text' => $text]);
        $request->session()->put('text', $text);

        $request->session()->push('arraytext', $text);

        return back();
    }

    public function sampleForget(Request $request)
    {
        $request->session()->forget('text');
        return back();
    }

    public function sampleForgetArray(Request $request, $key)
    {
        $x = $request->session()->pull("arraytext.$key");
        return back();
    }

    public function forgetCustomer(Request $request, $key)
    {
        $request->session()->pull("customer.$key");
        return back();
    }

    public function forgetFullCustomer(Request $request)
    {
        $request->session()->pull("customer");
        return back();
    }
}
