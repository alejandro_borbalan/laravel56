<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\Resource;

class Reservation extends Model
{
    protected $fillable =
    [
        'id', 'user_id', 'resource_id','date','limit',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function resources()
    {
        // dd(1);
        return $this->hasMany('App\Resource');
    }
}
