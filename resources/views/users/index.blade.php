@extends('layouts.app')

@section('content')

<div class="container">

<h1>Lista de Usuarios</h1>

    <table class="table table-bordered">
    <tr>
        <th>Id</th>
        <th>Nombre</th>
    </tr>
    @foreach ($users as $user)
    <tr>
        <td>{{ $user->id }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->role->name }}</td>
        <td><a class="btn btn-info" href="/users/{{ $user->id }}">Ver</a></td>
        <td><form method="post" action="/users/{{ $user->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input class="btn btn-info" type="submit" value="borrar"></form></td>
            </form>
        </td>
    </tr>
    @endforeach
</table>
{{ $users->links() }}

</div>
@endsection
