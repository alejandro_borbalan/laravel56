@extends('layouts.app')

@section('content')

<div class="container">

<h1>Detalle de Usuario</h1>

<ul>
    <li>Id: {{ $user->id }}</li>
    <li>Nombre: {{ $user->name }}</li>
    <li>E-mail: {{ $user->email }}</li>
    <li>Contraseña: {{ $user->password }}</li>
    <li>Rol: {{ $user->role->name }}</li>
</ul>


</div>
@endsection
