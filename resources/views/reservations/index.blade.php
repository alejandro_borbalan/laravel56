@extends('layouts.app')

@section('content')

<div class="container">

<h1>Lista de Reservas</h1>

    <table class="table table-bordered">
    <tr>
        <th>Id de la Reserva</th>
        <th>Propietario</th>
        <th>Recurso/s Asignados</th>
    </tr>
    @foreach ($reservations as $reservation)
    <tr>
        <td>{{ $reservation->id }}</td>

        <td>{{ $reservation->user->name }}</td>
        <td><a class="btn btn-info" href="/reservations/{{ $reservation->id }}">Ver</a></td>
        <td><form method="post" action="/reservations/{{ $reservation->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input class="btn btn-info" type="submit" value="borrar">

                <a class="btn btn-info" href="/reservations/{{ $reservation->id }}/edit">Editar</a></form></td>
            </form>
        </td>
    </tr>
    @endforeach
</table>

</div>
@endsection
