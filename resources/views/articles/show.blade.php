@extends('layouts.app')

@section('content')

<div class="container">

<h1>Detalle de artículo</h1>

<ul>
    <li>Código: {{ $article->code }}</li>
    <li>Nombre: {{ $article->name }}</li>
    <li>Precio: {{ $article->price }}</li>
</ul>


</div>
@endsection
