@extends('layouts.app')

@section('content')

    <div class="container">

        <h1>Alta de artículo</h1>

        <form method="post" action="/articles">
            {{ csrf_field() }}

            <div  class="form-group">
                <label>Código</label>
                <input class="form-control"  type="text" name="code" value="{{ old('code') }}">

                @if ($errors->first('code'))
                <div class="alert alert-danger">
                {{ $errors->first('code') }}
                </div>
                @endif
            </div>

            <div class="form-group">
                <label>Nombre</label>
                <input class="form-control"  type="text" name="name" value="{{ old('name') }}">
                @if ($errors->first('name'))
                <div class="alert alert-danger">
                {{ $errors->first('name') }}
                </div>
                @endif
            </div>

            <div class="form-group">
                <label>Precio</label>
                <input class="form-control"  type="text" name="price" value="{{ old('price') }}">
                @if ($errors->first('price'))
                <div class="alert alert-danger">
                {{ $errors->first('price') }}
                </div>
                @endif
            </div>

            <div class="form-group">
                <label></label>
                <input class="form-control"  type="submit" name="" value="Nuevo">
            </div>
        </form>

    </div>
@endsection
