@extends('layouts.app')

@section('content')

<div class="container">

<h1>Lista de artículos</h1>

    <table class="table table-bordered">
    <tr>
        <th>Código</th>
        <th>Nombre</th>
        <th>Precio</th>
        <th>Acciones</th>
    </tr>
    @foreach ($articles as $article)
    <tr>
        <td>{{ $article->code }}</td>
        <td>{{ $article->name }}</td>
        <td>{{ $article->price }}</td>
        <td>
            <form method="post" action="/articles/{{ $article->id }}">
            <a class="btn btn-success" href="/articles/remember/{{ $article->id }}">Recordar</a>

            @can ('view', $article)

            <a class="btn btn-info" href="/articles/{{ $article->id }}">Ver</a>
            @endcan
            @can ('update', $article)
            <a class="btn btn-info" href="/articles/{{ $article->id }}/edit">Editar</a>
            @endcan
            @can ('delete', $article)
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input class="btn btn-danger" type="submit" value="borrar">
            @endcan
            </form>
        </td>
    </tr>
    @endforeach
</table>
{{ $articles->links() }}

@can ('create', App\Article::class)
<a href="/articles/create" class="btn btn-info">Nuevo</a>
@endcan


<hr>

    <div>
        <h3>Lista de artículos</h3>
        <ul>
        @if (session()->has('articles'))
        @foreach (session()->get('articles') as $article)
            <li>{{ $article->code }} - {{ $article->name }}</li>
        @endforeach
        @endif
        </ul>
    </div>

    <div>
        <h3>Cesta de artículos con cantidad</h3>
        <ul>
        @if (session()->has('basket'))
        @foreach (session()->get('basket') as $item)
            <li>{{ $item['article']->code }} - {{ $item['article']->name }} :    Cantidad {{ $item['amount'] }}</li>
        @endforeach
        @endif
        </ul>
    </div>
</div>
@endsection
