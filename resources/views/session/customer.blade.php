@extends('layouts.app')

@section('content')

<div class="container">

<h1>Guardar texto en sesión</h1>

<form method="post" action="/sesion/customer">
    {{ csrf_field() }}

    <div  class="form-group">
        <label>Nombre</label>
        <input class="form-control"  type="text" name="name" value="">
    </div>

    <div  class="form-group">
        <label>Apellido</label>
        <input class="form-control"  type="text" name="surname" value="">
    </div>


    <div class="form-group">
        <label></label>
        <input class="form-control"  type="submit" name="" value="Nuevo">
    </div>

</form>

<div>
    <h2>Último Cliente:</h2>
    Nombre: {{ session()->get('customer.name') }}<br>
    Apellido: {{ session()->get('customer.surname') }} <br>

    <hr>

    @if (session()->has('customer'))
        @foreach ( session()->get('customer') as $key => $item)
            <li>
                {{ $key }}: {{ $item }}
                <a href="/sesion/forget_customer/{{ $key }}">Olvidar</a>
            </li>
        @endforeach
        <a href="/sesion/forget_full_customer">Olvidar customer</a>
    @else
        No hay nadie guardado
    @endif

</div>

    <hr>
    <div class="alert alert-default">
        <a class="btn btn-danger" href="/sesion/flush">Borrar datos de sessión: ojo que cerraremos al usuario logueado</a>
    </div>
</div>
@endsection
