@extends('layouts.app')

@section('content')

<div class="container">

<h1>Guardar Texto en sesión</h1>
<hr>
<div>
    <form method="post" action="/sesion/sample">
        {{ csrf_field() }}

        <div  class="form-group">
            <label>Texto</label>
            <input class="form-control"  type="text" name="text" value="">
        </div>

        <div class="form-group">
            <label></label>
            <input class="form-control"  type="submit" name="" value="Gurdar en sesión">
        </div>

    </form>
</div>

<div>
    <h2>Valor guardado:</h2>

        Último texto guardado en sesión (con Session):

        <strong>
        {{ Session::get('text') }}
        </strong>

        <hr>
        Último texto guardado en sesión (con request()):
        <strong>
        {{ request()->session()->get('text') }}
        </strong>

        <hr>
        Último texto guardado en sesión (con session()):
        {{ session('text') }}

</div>

<div>
        <h2>Array de textos:</h2>
        <ul>
        @if (request()->session()->has('arraytext'))
        @foreach (session::get('arraytext') as $key => $item)
            <li>{{ $item }}:   <a href="/sesion/sample_forget_array/{{ $key }}">Olvidar</a></li>
        @endforeach
        @endif
        </ul>
</div>


<div>
    <div class="alert alert-default">
        <a class="btn btn-warning" href="/sesion/sample_forget">Borrar datos selectivamente</a>
        <br>
        <br>
        <a class="btn btn-danger" href="/sesion/flush">Borrar datos de sessión: ojo que cerraremos al usuario logueado</a>
    </div>
</div>

    <hr>

</div>
@endsection
