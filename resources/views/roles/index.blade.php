@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <h1>Lista de roles</h1>

            @foreach($roles as $role)
                <li>{{ $role->name }} </li>
            @endforeach
        </div>
    </div>
</div>
@endsection
