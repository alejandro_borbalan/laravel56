<?php

use Faker\Generator as Faker;

$factory->define(App\Article::class, function (Faker $faker) {
    return [
        'code' => strtoupper(str_random(3) . rand(100, 999)),
        'name' => $faker->sentence(4, true),
        'price' => rand(1000,10000) / 100
    ];
});
