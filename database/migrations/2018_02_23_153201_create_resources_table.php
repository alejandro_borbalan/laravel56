<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesTable extends Migration
{
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('name');
            //$table->integer('reservation_id')->nullable()->unsigned();
           // $table->foreign('reservation_id')->references('id')->on('reservation_id');
            $table->date('date');
            $table->date('limit');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('resources');
    }
}
