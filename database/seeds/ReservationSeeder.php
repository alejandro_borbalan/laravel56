<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ReservationSeeder extends Seeder
{
    public function run()
    {
         DB::table('reservations')->insert([
                    'user_id' => 1,
                    'resource_id' => 1,
                    'date' => Carbon::parse('2017-01-01'),
                    'limit' => Carbon::parse('2018-01-01'),
                ]);

         DB::table('reservations')->insert([
                    'user_id' => 2,
                    'resource_id' => 2,
                    'date' => Carbon::parse('2017-01-01'),
                    'limit' => Carbon::parse('2018-01-01'),
                ]);

         DB::table('reservations')->insert([
                    'user_id' => 2,
                    'resource_id' => 3,
                    'date' => Carbon::parse('2017-01-01'),
                    'limit' => Carbon::parse('2018-01-01'),
                ]);
         DB::table('reservations')->insert([
                    'user_id' => 3,
                    'resource_id' => 4,
                    'date' => Carbon::parse('2017-01-01'),
                    'limit' => Carbon::parse('2018-01-01'),
                ]);
    }
}
