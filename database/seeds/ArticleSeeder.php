<?php

use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    public function run()
    {
        DB::table('articles')->insert(
        [
            'code' => 'TEC001',
            'name' => 'Teclado USB',
            'price' => 13.99,
        ]);

        factory(App\Article::class, 10)->create();
    }
}
