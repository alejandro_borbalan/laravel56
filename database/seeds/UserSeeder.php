<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {

        DB::table('users')->insert([
            'name' => 'Alex',
            'email' => 'alex@alex.alex',
            'password' => bcrypt('alex'),
            'role_id' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'Pepe',
            'email' => 'pepe@daw.com',
            'password' => bcrypt('pepe'),
            'role_id' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'Ana',
            'email' => 'ana@daw.com',
            'password' => bcrypt('ana'),
            'role_id' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'Juan',
            'email' => 'juan@daw.com',
            'password' => bcrypt('juan'),
            'role_id' => 2,
        ]);

        DB::table('users')->insert([
            'name' => 'Sonia',
            'email' => 'sonia@daw.com',
            'password' => bcrypt('sonia'),
            'role_id' => 2,
        ]);

        factory(App\User::class, 10)->create();
    }
}
