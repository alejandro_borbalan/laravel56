<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ResourceSeeder extends Seeder
{
    public function run()
    {
        DB::table('resources')->insert([
                  'id' => 1,
                  'name' => "producto 1",
                  'date' => Carbon::parse('2017-01-01'),
                  'limit' => Carbon::parse('2018-01-01'),
                ]);

         DB::table('resources')->insert([
                   'id' => 2,
                   'name' => "producto 2",
                   'date' => Carbon::parse('2017-01-01'),
                   'limit' => Carbon::parse('2018-01-01'),
                ]);

        DB::table('resources')->insert([
                  'id' => 3,
                  'name' => "producto 3",
                  'date' => Carbon::parse('2017-01-01'),
                  'limit' => Carbon::parse('2018-01-01'),
                ]);

        DB::table('resources')->insert([
                  'id' => 4,
                  'name' => "producto 4",
                  'date' => Carbon::parse('2017-01-01'),
                  'limit' => Carbon::parse('2018-01-01'),
                ]);
    }
}
