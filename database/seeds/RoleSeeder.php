<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'administrador',
        ]);

        DB::table('roles')->insert([
            'name' => 'usuario',
        ]);
    }
}
